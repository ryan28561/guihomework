class Student extends Person {
    private String classStatus;
    public Student(String name, String address, String phone, String email, String classStatus){
        super(name,address,phone,email);
        this.classStatus = classStatus;
    }

    public String getClassStatus(){
        return this.classStatus;
    }

    public String toString(){
        return "Class: Student -- Name: " + this.getName();
    }
}
