
class TestPerson {
    public static void main(String[] args) {
        Person p = new Person("steve", "address thingy", "Phone number", "email@email.com");
        Student s = new Student("Student One", "1234 student st", "9999999999", "email2@email.com", "Freshman");
        Employee e = new Employee("Employee Name", "123 Employee St.", "123-123-1233", "email3@email.com", "Cool office", 30000, new MyDate(0,0,2016));
        Faculty f = new Faculty("Faculty Name", "123 Faculty St.", "123-123-1233", "email4@email.com", "Lame office", 10000, new MyDate(1,1,2015), "Never to never", "Best");
        Staff st = new Staff("Staff Name", "123 Staff St.", "123-123-1233", "email5@email.com", "Another office", 500000, new MyDate(2,2,2019), "Supreme Overlord");

        System.out.println("== Print all the classes created ==\n");
        System.out.println(p);
        System.out.println(s);
        System.out.println(e);
        System.out.println(f);
        System.out.println(st);

        System.out.println("\n\n== Testing Name setter ==\n");
        System.out.println(p);
        p.setName("Not Steve anymore..");
        System.out.println(p);

        System.out.println("\n\n== Testing student class getter ==\n");
        System.out.println(s.getClassStatus());
        
        System.out.println("\n\n== Testing Employee office getter ==\n");
        System.out.println(e.getOffice());

        System.out.println("\n\n== Testing Faculty rank getter ==\n");
        System.out.println(f.getRank());

        System.out.println("\n\n== Testing Staff title getter ==\n");
        System.out.println(st.getTitle());

    }
}
