class Faculty extends Employee{
    private String officeHours;
    private String rank;

    public Faculty(String name,
                   String address,
                   String phone,
                   String email,
                   String office,
                   int salary,
                   MyDate date,
                   String hours,
                   String rank){
        super(name, address, phone, email, office, salary, date);
        this.officeHours = hours;
        this.rank = rank;
    }

    public String getOfficeHours(){
        return this.officeHours;
    }
    public String getRank(){
        return this.rank;
    }

    public void setOfficeHours(String hours){
        this.officeHours = hours;
    }
    public void setRank(String rank){
        this.rank = rank;
    }

    public String toString(){
        return "Class: Faculty -- Name: " + this.getName();
    }
}
