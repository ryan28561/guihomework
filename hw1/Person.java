public class Person {
    private String name;
    private String address;
    private String phonenumber;
    private String emailaddress;
    
    public Person(String name, String address, String phone, String email){
        this.name = name;
        this.address = address;
        this.phonenumber = phone;
        this.emailaddress = email;
    }

    public String getName(){
        return this.name;
    }
    public String getAddress(){
        return this.address;
    }
    public String getPhoneNo() {
        return this.phonenumber;
    }
    public String getEmailAddress() {
        return this.emailaddress;
    }

    public void setName(String name){
        this.name = name;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public void setPhone(String number){
        this.phonenumber = number;
    }
    public void setEmail(String email){
        this.emailaddress = email;
    }

    public String toString(){
        return "Class: Person -- Name: " + this.name;
    }
}
