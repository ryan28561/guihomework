public class Employee extends Person {
    private String office;
    private int salary;
    private MyDate dateHired;
    
    public Employee(String name, String address, String phone, String email, String office, int salary, MyDate dateHired){
        super(name, address, phone, email);
        this.office = office;
        this.salary = salary;
        this.dateHired = dateHired;
    }

    public String getOffice(){
        return this.office;
    }
    public int getSalary(){
        return this.salary;
    }
    public MyDate getDateHired(){
        return this.dateHired;
    }

    public void setOffice(String office){
        this.office = office;
    }
    public void setSalary(int salary){
        this.salary = salary;
    }
    public void setDateHired(MyDate date){
        this.dateHired = date;
    }

    public String toString(){
        return "Class: Employee -- Name: " + this.getName();
    }
}   
